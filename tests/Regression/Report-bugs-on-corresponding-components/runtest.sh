#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/setroubleshoot/Regression/Report-bugs-on-corresponding-components
#   Description: Can sealert identify source RPM of AVC domain type?
#   Author: Vit Mojzis <vmojzis@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2020 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="setroubleshoot"

if [ -z "${TEST_PACKAGES+set}" ];
then PACKAGES=(flatpak-selinux tpm2-abrmd-selinux container-selinux usbguard-selinux mysql-selinux fapolicyd-selinux)
else PACKAGES=(${TEST_PACKAGES[@]})
fi

#corresponding module names
#MODULES=(flatpak tabrmd     container usbguard mysql)

# <rpm package> - <selinux module> - <domain type>
# flatpak-selinux - flatpak - flatpak_helper_t
# tpm2-abrmd-selinux - tabrmd - tabrmd_t
# container-selinux - container - docker_t
# usbguard-selinux - usbguard - usbguard_t      -- fedora only
# mysql-selinux - mysql - mysql_t              -- fedora only


rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        OUTPUT_FILE=`mktemp`
        # Package installation is handled by Makefile for now
        # install availlable policy packages
        # for RPM in ${PACKAGES[@]};
        # do
        #     sudo dnf install -y ${RPM} || continue
        # done
    rlPhaseEnd

    rlPhaseStartTest
        for RPM in ${PACKAGES[@]};
        do
            # run only for policies that are installed
            rpm -q ${RPM} >& /dev/null
            if [ $? -ne 0 ]; then echo "${RPM} not installed! Skipping.";  continue; fi
            rlRun "sealert -a ./avc_${RPM} 2>&1 | tee ${OUTPUT_FILE} | grep \"Local Policy RPM\""
            if [ $? -ne 0 ]; then cat ${OUTPUT_FILE}; fi
            # test if correct rpm was identified
            rlRun "grep -i \"Local Policy RPM\" ${OUTPUT_FILE} | grep \"$RPM\S*$\" -o"
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rm -f ${OUTPUT_FILE}
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
