#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/setroubleshoot-plugins/Sanity/public_content
#   Description: Does the plugin work as expected?
#   Author: Milos Malik <mmalik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="setroubleshoot-plugins"
PLUGIN_NAME="public_content"
ANALYSIS_DELAY=${ANALYSIS_DELAY:-"16"}

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm ${PACKAGE}
        rlAssertRpm setroubleshoot-server
        rlServiceStart auditd
        rlFileBackup /var/lib/setroubleshoot/setroubleshoot_database.xml
        rlRun "rm -f /var/lib/setroubleshoot/setroubleshoot_database.xml"
        BEFORE=`mktemp`
        AFTER=`mktemp`
        rlRun "killall setroubleshootd" 0,1
        rlRun "mkdir -p /var/test-dir"
        rlRun "chcon -t samba_share_t /var/test-dir"
        rlRun "touch /var/test-file"
        rlRun "chcon -t samba_share_t /var/test-file"
    rlPhaseEnd

    rlPhaseStartTest
        if rlIsRHEL 6 7 ; then
            ADD_OPT="-C"
        fi
        rlRun "sesearch -s rsync_t -t samba_share_t -c dir -p read -A --dontaudit ${ADD_OPT} | grep -v '\]' | grep -e allow -e dontaudit" 1
        rlRun "sesearch -s rsync_t -t samba_share_t -c file -p read -A --dontaudit ${ADD_OPT} | grep -v '\]' | grep -e allow -e dontaudit" 1
        rlRun "setsebool rsync_export_all_ro off"
        if ! rlIsRHEL 6 ; then
            rlRun "setsebool rsync_full_access off"
        fi
        rlRun "sealert -l '*' > ${BEFORE}"
        rlRun "cat /var/log/messages > ./messages"
        rlRun "setenforce 0"
        rlRun "runcon system_u:system_r:rsync_t:s0 bash -c 'ls /var/test-dir'" 0,1
        rlRun "runcon system_u:system_r:rsync_t:s0 bash -c 'cat /var/test-file'" 0,1
        rlRun "setenforce 1"
        rlRun "sleep ${ANALYSIS_DELAY}"
        rlRun "ps -efZ | grep setroubleshootd" 0,1
        rlRun "sealert -l '*' > ${AFTER}" 0-3
        rlRun "ausearch -m avc -m selinux_err -i -ts recent --input-logs | grep 'read.*ls.*test-dir.*:rsync_t:.*:samba_share_t:.*tclass=dir'"
        rlRun "ausearch -m avc -m selinux_err -i -ts recent --input-logs | grep 'read.*cat.*test-file.*:rsync_t:.*:samba_share_t:.*tclass=file'"
        rlRun "diff ${BEFORE} ${AFTER} | grep \"Plugin.*suggests\""
        rlRun "diff ${BEFORE} ${AFTER} | grep \"Plugin ${PLUGIN_NAME} .*suggests\""
        rlRun "diff /var/log/messages ./messages | grep -i -e 'setroubleshoot.*exception' -e 'no such file or directory'" 1
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm -rf /var/test-dir /var/test-file"
        rm -f ${BEFORE}
        rm -f ${AFTER}
        rlFileRestore
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

